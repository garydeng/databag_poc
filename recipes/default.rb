#
# Cookbook Name:: databag_poc
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#
#

#########################################################################################################
#use the secrete key default server. no need to specify the location of the key
#by default the key is at: /etc/chef/encrypted_data_bag_secret 
#########################################################################################################
user_bag= Chef::EncryptedDataBagItem.load('databag_poc_plain', "#{node.chef_environment}")

#########################################################################################################
#use the secrete key generated in local, save the key within the cookbook. Then we copy it to /tmp and refernce it there
#the command to encrypt the data bag:
#knife data bag from file databag_poc_local ecomm-sandbox.json --secret-file /opt/chef/chef_data/cookbooks/databag_poc/files/default/secret/cookbook_data_bag_secret
#########################################################################################################
copy_secret = remote_directory "/tmp" do
  source "secret"
  files_owner "root"
  files_group "root"
  files_mode 00644
  owner "nobody"
  group "nobody"
  mode 00755
end
copy_secret.run_action(:create)

#fetch the user info by the secret key inside cookbook 
secret_local = Chef::EncryptedDataBagItem.load_secret("/tmp/cookbook_data_bag_secret")
user_local= Chef::EncryptedDataBagItem.load('databag_poc_local', "#{node.chef_environment}", secret_local)



#########################################################################################################
#use the secrete key generated in local, save the key in artifactory server. Then we copy it to /tmp and refernce it there
#the command to encrypt the data bag:
#knife data bag from file databag_poc_local ecomm-sandbox.json --secret-file /opt/chef/chef_data/cookbooks/databag_poc/files/default/secret/poc_data_bag_secret
#the artifactory server url: http://scholastic.artifactoryonline.com/scholastic/simple/Certificates/secret/poc_data_bag_secret
#########################################################################################################
artifactory_db = Chef::EncryptedDataBagItem.load("services_artifactory_default", "ecomm-sandbox",)
username = artifactory_db['artifactory_username']
password = artifactory_db['artifactory_password']
remote_secret = remote_file "/tmp/poc_data_bag_secret" do
   source "http://scholastic.artifactoryonline.com/scholastic/simple/Certificates/secret/poc_data_bag_secret"
   headers ({"AUTHORIZATION" => "Basic #{Base64.encode64("#{username}:#{password}")}"})
   owner "root"
   group "root"
   mode 0755
end
remote_secret.run_action(:create)

#fetch the user info by the secret key from artifactory 
secret_remote = Chef::EncryptedDataBagItem.load_secret("/tmp/poc_data_bag_secret")
user_remote= Chef::EncryptedDataBagItem.load('databag_poc_local', "#{node.chef_environment}", secret_remote)


########################################################################################################
# create data bag with Ruby
# ######################################################################################################
ruby_block "create databab and insert items" do
  block do
    mydatabag = Chef::DataBag.new
    mydatabag.name("databag_poc_test_data")
    mydatabag.save

    data = {
      'id'=> 'SampleUserName',
      'seed'=> node.ipaddress
    }
    item = Chef::DataBagItem.new
    item.data_bag('databag_poc_test_data')
    item.raw_data = data
    item.save
  end
  action :create
end

########################################################################################################
# Print the results to a temp text file
# ######################################################################################################
template "/tmp/databagtest.txt" do
  source "databagtest.erb"
  mode 0644
  owner "root"
  group "root"

  variables ({
    #user info got from cookbook
    :user => node['databag_poc']['user'],
    :password => node['databag_poc']['passwd'],

    #user info got from chef server 
    :user_plain => user_bag['username_plain'],
    :password_plain => user_bag['password_plain'],

    #user info got from local secret key(file system path) 
    :user_local => user_local['username_local'],
    :password_local => user_local['password_local'], 

    #user info got from remote secret key(http path) 
    :user_remote => user_remote['username_remote'],
    :password_remote => user_remote['password_remote'] 
  })
end
