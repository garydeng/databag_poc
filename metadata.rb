name             'databag_poc'
maintainer       'Scholastic'
maintainer_email 'gdeng@scholastic.com'
license          'All rights reserved'
description      'Installs/Configures databag_poc'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

#global
